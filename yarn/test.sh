#!/bin/bash

yarn install --ignore-engines
yarn licenses list --no-progress --json | jq '.'
