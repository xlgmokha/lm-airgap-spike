#!/bin/bash

rm -fr build
mvn org.codehaus.mojo:license-maven-plugin:download-licenses
cat target/generated-resources/licenses.xml
