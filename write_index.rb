#!/bin/env ruby

require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'pg'
  gem 'msgpack'
end

require 'pg'
require 'digest'
require 'yaml'
require 'msgpack'

index = {}

connection = PG.connect(host: File.expand_path('tmp/sockets'), dbname: 'postgres')
connection.exec("select full_name, licenses from versions where licenses is not null") do |result|
  result.each do |row|
    index[row['full_name']] = YAML.safe_load(row['licenses'])
  end
end

puts index.inspect
puts "creating index file"
File.open('rubygems.index', 'w') do |file|
  packer = MessagePack::Packer.new(file)
  packer.write(index)
  packer.flush
end
puts `ls -alh rubygems.index`
copy_of_index = MessagePack.unpack(IO.binread('rubygems.index'))
puts ['equals?', index == copy_of_index].inspect
