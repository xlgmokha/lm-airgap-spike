#!/bin/bash

pip install conan
rm -fr build
mkdir -p build && cd build || exit 1
conan install ..
cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release
cmake --build .
./bin/md5

conan info ..
grep -rn 'license = "' $HOME/.conan/data/
