# License Scanning - Airgap

This repository contains example projects that can be used for
testing license scanning in an airgap mode.

Our [License scanner](https://gitlab.com/gitlab-org/security-products/license-management)
depends on [LicenseFinder](https://github.com/pivotal/LicenseFinder/) to perform the scan.

License Finder depends on the native dependency managers to provide information on
the different licenses associated with dependencies identified by the dependency manager.

## Prerequisites

Some of these configurations depends on projects having the proper plugins added to the project.
All configurations require that the dependencies are installed before the scan takes place.
Scanning for licenses cannot function in most cases without the project dependencies already installed.
In out CI scenario this requires that customers use the
[SETUP_CMD](https://docs.gitlab.com/ee/user/application_security/license_compliance/#available-variables)
to install packages in a way that works in their airgap environment.

## Dependency Managers

The dependency managers that we support are:

* [Bower - JavaScript](#Bower)
* [Bundler - Ruby](#Bundler)
* [Cargo - Rust](#Cargo)
* [Carthage - MacOS/iOS](#Carthage)
* [CocoaPods - Swift/Objective-C](#CocoaPods)
* [Composer - PHP](#Composer)
* [Conan - C++](#Conan)
* [Gradle - Java](#Gradle)
* [Maven - Java](#Maven)
* [Mix - Elixir](#Mix)
* [NPM - JavaScript](#NPM)
* [Nuget - .NET](#Nuget)
* [Pip - Python](#Pip)
* [Rebar - Erlang](#Rebar)
* [Yarn - JavaScript](#Yarn)
* [dotnet - .NET](#dotnet)
* [sbt - Scala](#sbt)

## Bower

* `bower.json`
* `bower`
* `bower install`
* `bower list --json -l action --allow-root`
  

After dependencies are installed the licenses can be found in `bower_components/**/.bower.json`

```bash
$ grep license bower_components/**/.bower.json
  "license": "MIT",
```

## Bundler

* `bundle`
* `bundle install`
* `Gemfile`

All metadata is parsed from the installed *.gemspec files

## Cargo

* `cargo`
* `cargo fetch`
* `Cargo.lock` | `Cargo.toml`
* `cargo metadata --format-version=1`
  
Cargo delegates to the crates.io index to find the metadata for a file.
The license info isn't in the [index](https://github.com/rust-lang/crates.io-index)
so it attempts to fetch the missing metadata using the network.
There is an [--offline](https://doc.rust-lang.org/cargo/commands/cargo-metadata.html)
option to prevent `cargo` from accessing the network
but this isn't used in [LicenseFinder](https://github.com/pivotal/LicenseFinder/blob/de82cff7a99985bd95b54442479e345d83fea3a0/lib/license_finder/package_managers/cargo.rb#L28).

## Carthage

* `carthage`
* `Cartfile`
* `Cartfile.resolved`

A dependency manager for Cocoa.

## CocoaPods

* `Podfile`
* `Podfile.lock`

## Composer

* `composer.lock`
* `composer.json`
* `composer install`
* `composer licenses --format=json`

If the composer dependencies are installed it looks like the necessary metadata to
parse the license information can be found from the `composer.json` files
for each dependency.

```bash
$ grep -rn 'license\":' vendor/composer/**/composer.json
5:    "license": "MIT",
```

## Conan

* `conanfile.txt`
* `conan install .`
* `conan info .`

Conan dependency metadata is stored in `$HOME/.conan/data`

## dotnet

* `.csproj`
* `dotnet restore`
* `project.assets.json` files are discovered in the `project/obj/` directory

A `project.assets.json` file is used to locate the list of dependencies and where their `nuspec` file is.
The `nuspec` file is parsed for the license info for the dependency.

## Gradle

* `gradlew downloadLicenses`
* `build.gradle`
* `build.gradle.kts`
* `settings.gradle`

Gradle depends on a plugin to provide a task called `downloadLicenses`.

The following must be added to a `.gradle` file.


```gradle
plugins {
  id "com.github.hierynomus.license" version "0.15.0"
}
```

This plugin uses the gradle project API (`import org.gradle.api.Project`)
to resolve the list of dependencies via [configuration.resolve()](https://docs.gradle.org/current/javadoc/org/gradle/api/artifacts/Configuration.html#resolve--)

> This locates and downloads the files which make up this configuration, and returns the resulting set of files.

It looks this might be making network requests to resolve the list of dependencies to parse their licenses.

[Here](https://github.com/hierynomus/license-gradle-plugin/blob/25b456513aed45c725b657655f2ba0527b5341d2/src/main/groovy/nl/javadude/gradle/plugins/license/LicenseResolver.groovy#L238).

It then parses the `build/reports/license/dependency-license.xml` file.

## Maven

* `mvn`
* `mvn org.codehaus.mojo:licenses-maven-plugin:download-licenses`
* `pom.xml`
* `mvn help:evaluate`

Searches for a `pom.xml` and uses a plugin to download the licenses for the identified dependencies.
Then parses the `target/generated-resources/licenses.xml`.

The license files are downloaded [here](https://github.com/mojohaus/license-maven-plugin/blob/01fd9a1c560fb608e721de0cb1673a29805a40cd/src/main/java/org/codehaus/mojo/license/download/LicenseDownloader.java#L201).

## Mix

* `mix deps.get`
* `mix.lock`
* `mix deps`

After `mix` dependencies are installed to `dep` a search through `deps/**/hex_metadata.config` is used
to parse the license for each of the installed dependencies.

## NPM

* `npm install`
* `package.json`
* `npm list --json --long`

`npm install` is used to install the packages then `npm list --json --long` is used to parse
the license information from the package metadata.

## Nuget

* `packages.config`
* `vendor/*.nupkg`
* `.nuget/`
* `*.sln`
* `.nupkg`
* `.nuspec`
* `mono /usr/local/bin/nuget.exe restore`

Use `nuget restore` to install dependencies, then unzip `*.nupkg` files to parse out the `*.nuspec`. Assumes,
that dependencies are installed under project root directory but this needs to be configured via `-PackagesDirectory`
option which doesn't appear to be in use in the [LicenseFinder](https://github.com/pivotal/LicenseFinder/blob/de82cff7a99985bd95b54442479e345d83fea3a0/lib/license_finder/package_managers/nuget.rb#L81) code.
This means dependencies are likely installed in the default directory which is [$HOME/.nuget/packages](https://docs.microsoft.com/en-us/nuget/reference/nuget-config-file#config-section).
I think we need to make sure that dependencies are installed under the project root so that they can be discovered in license detection.

## Pip

* `pip2`
* `pip3 install -r requirements.txt`
* `requirements.txt`
* Delegates to python script to use `PipSession` to download license info.

`pip` relies on pypi.org to fetch license metadata for each dependency. Also, the support for
parsing `Pipfile.lock` also depends on querying the `pypi.org` API to fetch missing metadata.

Possible options to address this is:

* build specialized offline index of python dependency name/version to SPDX license expression.
* Do not support python project license scanning. We can report dependencies but not licenses.
* Parse installed metadata files for license info. Assumes that project dependencies are already installed
  * My initial scanning of the installed dependencies did yield some license info in files like `dist-info/metadata.json` and `dist-info/METADATA`. However, it doesn't appear that all dependencies have this info. Needs more research.

## Rebar

* `rebar`
* `rebar.config`
* `rebar list-deps`

The latest version of [rebar](https://www.rebar3.org/docs/getting-started) appears to be `rebar3`.
This version doesn't seem to support the `list-deps` command.

`rebar` version 2.6.4 does support the `list-deps` command but it does not include license info.
From reading the `rebar` [code](https://github.com/pivotal/LicenseFinder/blob/de82cff7a99985bd95b54442479e345d83fea3a0/lib/license_finder/package_managers/rebar.rb#L13-L19) 
in LicenseFinder it looks like license info is not supplied. This means that LicenseFinder
will fallback to searching for license files from the project root. I was unable to find
any license files in the `deps` directory where dependencies were installed to.

I wasn't able to generate a report with licenses for `rebar` and `rebar3` does not support
the `list-deps` command..

## sbt

* `sbt dumpLicenseReport`
* `build.sbt`
* Appears to use SbtIvy to resolve dependencies. I can't tell if this makes network requests or not. https://github.com/sbt/sbt-license-report/blob/master/src/main/scala/com/typesafe/sbt/SbtLicenseReport.scala

The `scala sbt` integration requires that a project install the [sbt-license-report plugin]()
in order to run `sbt dumpLicenseReport`. Projects that do not have this plugin will not be
able to generate a license report using LicenseFinder.

The `sbt dumpLicenseReport` command appears to be downloading license information at runtime.

## Yarn

* `yarn.lock`
* `yarn install --ignore-engines`
* `yarn licenses list --no-progress --json`

Using yarn dependencies need to be installed before the licenses can be listed.
the `yarn licenses list` command has a `--offline` option to prevent making outbound network
requests but LicenseFinder does not use this. [code](https://github.com/pivotal/LicenseFinder/blob/de82cff7a99985bd95b54442479e345d83fea3a0/lib/license_finder/package_managers/yarn.rb#L5)

## Go dependency managers

* Go dep via 
  * `Gopkg.lock`
  * `dep ensure -vendor-only`
  * `dep`
* glide
  * `glide install`
  * `glide.lock`
* Go15VendorExperiment
  * `go list std`
  * `vendor/*.go`
* GoDep
  * `GoDeps/Godeps.json`
  * `godep`
* GoModules
  * `go mod tidy`
  * `go mod vendor`
  * `go list -m`
* GoWorkspace
  * `go list -f`
* GoVendor
  * `vendor/vendor.json`
  * `govendor sync`
* Gvt
  * `gvt restore`
  * `gvt list -f`
* trash
  * `trash`
  * `trash.lock`

## Offline index

A possible solution for airgap friendly license scanning
is to produce an offline index that specifies what
licenses are associated with a specific dependency and version.

The [SPDX Catalogue](https://spdx.org/licenses/) currently defines 402 licenses.
Some of these licenses are now deprecated.

```bash
モ curl -s https://spdx.org/licenses/licenses.json | jq '.licenses[].licenseId' | wc -l
402
```
### How many items would we need to index?

* rubygems:(157,000 gems)
* nuget: (186,000 packages)
* maven:
  * 322,000 packages
  * 4,700,000 artifacts
* npmjs.com
  * 1,197,893 packages
* pypi.org
  * 216,076 packages
  * 1,653,137 releases

NPM appears to be the biggest package registry. So that will work well for a worst case
scenario calculation.

If we build an index for each package registry separately we can consider the following
worst case scenarios for each registry.

* max packages: ~2M
* max 3 licenses per package/version
* average 10 versions per package

Querying the rubygems dataset shows that packages seem to average around 6.7 versions.
```sql
# select avg(x) from (select count(rubygem_id) as x from versions group by rubygem_id order by count(rubygem_id) desc) z;
        avg
--------------------
 6.7848474733868180
(1 row)

Time: 528.080 ms
```

Max # of versions for a gem.

```sql
# select count(rubygem_id) from versions group by rubygem_id order by count(rubygem_id) desc limit 5;
 count
-------
  2933
  1935
  1475
  1414
  1316
(5 rows)

Time: 478.490 ms
```

This repos includes a couple of ruby scripts that unpacks the latest data dump
of the rubygems database from [here](https://rubygems.org/pages/data) and turns that
into a single flat file index serializing a hash table using [MessagePack](https://msgpack.org/index.html).

The index only includes records for gems that have specified a license.
**This index contains 912,283 entries for a total file size of 23M.**

```bash
$ ruby read_index.rb
```

I think building an offline index is feasible for handling airgap scenarios.
There will be some lag between rebuilds of the index. Some package registries
expose indexes as a git repository, database or API so these different sources
will need to be treated separately.

## Conclusion

Technically none of the project types are airgap friendly unless
customers can install dependencies before initiating a scan. This
requires use of the [SETUP_CMD](https://docs.gitlab.com/ee/user/application_security/license_compliance/#available-variables) or a `before_script`.

Even if the dependencies can be installed then the following
project types fail airgap testing due to scantime outbound network
requests.

I have identified the following issues that needs further investigation:

* [ ] Rebar: rebar3 is not supported.
* [ ] Gradle: Requires that the project have a plugin installed.
* [ ] Maven: Uses a plugin to download missing license info at scan time.
* [ ] sbt: Depends on a plugin to be installed in project settings.

The following project types are currently not airgap friendly.

* [ ] Cargo: requires [--offline](https://doc.rust-lang.org/cargo/commands/cargo-metadata.html) flag.
* [ ] Gradle: plugin appears to download license info at scan time.
* [ ] Maven: plugin appears to download license info at scan time.
* [ ] Nuget: The current core requires that the dependencies are installed under the project root otherwise it looks airgap friendly.
* [ ] Pip: Depends on `pypi.org` API at scan time.
* [ ] sbt: plugin appears to download licenses at scan time

I think more in-depth testing can be performed by setting up a proxy server
to record all outbound requests during scan time to identify which package
managers are making outbound requests vs scanning dependency metadata.
