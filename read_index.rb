#!/bin/env ruby

require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'msgpack'
end

require 'msgpack'

index = MessagePack.unpack(IO.binread('rubygems.index'))

index.each do |key, value|
  puts [key, value].inspect
end
puts ['Total', index.keys.count].inspect
