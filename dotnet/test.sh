#!/bin/bash

dotnet restore --no-cache --packages ./packages --verbosity diagnostic
cat obj/project.assets.json  | jq '.'
