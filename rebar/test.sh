#!/bin/sh

echo '-----------'
echo 'test rebar'
echo '-----------'
rebar --version
rebar get-deps
rebar list-deps

echo '-----------'
echo 'test rebar3'
echo '-----------'
cd myrelease
if [ ! -f rebar3 ]; then
  wget https://s3.amazonaws.com/rebar3/rebar3 && chmod +x rebar3
fi

./rebar3 --version
./rebar3 local install
./rebar3 list-deps
